function namevalidate(){

  var regName = /^[a-zA-Z]+ [a-zA-Z]+ [a-zA-Z]+$/;

  var name = document.getElementById('name').value;
  if(!regName.test(name)){
      alert('Name should only accept letters and spaces.');
      document.getElementById('name').focus();
      return false;
  }else{
      return true;
  }
}