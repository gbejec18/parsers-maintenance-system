<?php
$msg = "";
define('TITLE', 'Submit Request');
define('PAGE', 'SubmitRequest');
include('includes/header.php');
include('../dbConnection.php');
session_start();
if($_SESSION['is_login']){
    $rEmail = $_SESSION['rEmail'];
} else {
    echo "<script> location.href='RequesterLogin.php'</script>";
}

if(isset($_REQUEST['submitrequest'])){
    // Checking for empty fields
    if(($_REQUEST['requesttype'] == "") || ($_REQUEST['requestdesc'] == "") || ($_REQUEST['requestername'] == "") || ($_REQUEST['requesterlabnum'] == "") || ($_REQUEST['requesteremail'] == "") || ($_REQUEST['requestermobile'] == "") || ($_REQUEST['requestdate'] == "")){
   $msg = "<div class='alert alert-warning col-sm-6 ml-5 mt-2'>Fill All Fields</div>";
    } else {
        $rinfo = $_REQUEST['requesttype'];
        $rdesc = $_REQUEST['requestdesc'];
        $rname = $_REQUEST['requestername'];
        $rlabnum = $_REQUEST['requesterlabnum'];
        $remail = $_REQUEST['requesteremail'];
        $rmobile = $_REQUEST['requestermobile'];
        $rdate = $_REQUEST['requestdate'];
        $sql = "INSERT INTO submitrequest_tb(request_info, request_desc, requester_name, requester_labnumb, requester_email, requester_mobile, request_date)VALUES('$rinfo', '$rdesc', '$rname', '$rlabnum', '$remail', '$rmobile', '$rdate' )";
        if($conn->query($sql) == TRUE){
            $genid = mysqli_insert_id($conn);
            $msg = "<div class='alert alert-success col-sm-6 ml-5 mt-2'>Request Submitted Successfully</div>";
            $_SESSION['myid'] = $genid;
            echo "<script> location.href='submitrequestsuccess.php'</script>";
        } else {
            $msg = "<div class='alert alert-danger col-sm-6 ml-5 mt-2'>Unable to Submit Your Request</div>";
        }
    }
}

?>
    
<div class="col-sm-9 col-md-10 mt-5">   <!-- Start Service Request Form 2nd Column -->
<form class="mx-5" action="" method="POST" name = "frm" onsubmit="return namevalidate()" value="Validate Name">
<div class="form-group">
<?php if(isset($msg)){echo $msg;} ?>
<label for="inputRequestInfo">Request Type</label>
<select name="requesttype" >
    <option value="">Select</option>
    <option value="Electrical Problem"> Electrical Problem </option>
    <option value="Electrical Problem"> Technical Problem </option>
</select>
</div>
<div class="form-group">
<label for="inputRequestDescription">Description</label>
<input type="text" class="form-control" id="inputRequestDescription" placeholder="Write Description" name="requestdesc">
</div>
<div class="form-group">
<label for="inputName">Name</label>
<input type="text" class="form-control" id="inputName" placeholder="Enter Name" name="requestername">
</div>
<div class="form-group">
<label for="inputName">Laboratory Number</label>
<input type="text" class="form-control" id="inputlabNum" placeholder="Enter Laboratory Number" name="requesterlabnum">
</div>
<div class="form-row">
<div class="form-group col-md-6">
<label for="inputEmail">Email</label>
<input type="email" class="form-control" id="inputEmail" name="requesteremail" placeholder="Enter Email">
</div>
<div class="form-group col-md-2">
<label for="inputMobile">Mobile Number</label>
<input type="text" class="form-control" id="inputMobile" name="requestermobile" placeholder="Enter Mobile Number">
</div>
<div class="form-group col-md-2">
<label for="inputDate">Date</label>
<input type="date" class="form-control" id="inputDate" name="requestdate">
</div>
</div>
<button type="submit" class="btn btn-danger" name="submitrequest" onclick="return val()">Submit</button>
<button type="reset" class="btn btn-secondary">Reset</button>
</form>
</div> <!-- End Service Request Form 2nd Column -> 


    <!- Only Number for Input Fields -->
<script>
     function val () {
            if(frm.requestermobile.value=="")
            {
                alert("Please Enter Phone number");
                frm.requestermobile.focus();
                return false;
            }
            if(isNaN(frm.requestermobile.value))
            {
                alert("Invalid phone number");
                frm.requestermobile.focus();
                return false;
            }
            if((frm.requestermobile.value).length < 11)
            {
                alert("Phone number should be 11 digits");
                frm.requestermobile.focus();
                return false;
            }
            return true;
    }
    function namevalidate(){

var regName = /^[a-zA-Z]+ [a-zA-Z]+ [a-zA-Z]+$/;

var inputName = document.getElementById('inputName').value;
if(!regName.test(inputName)){
    alert('Name should only accept letters and spaces');
    document.getElementById('inputName').focus();
    return false;
}else{
    return true;
}
    }
</script>
<?php
include('includes/footer.php');
?>
